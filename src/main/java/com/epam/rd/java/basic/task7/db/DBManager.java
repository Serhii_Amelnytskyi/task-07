package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class DBManager {

    private static final String SQL_SELECT_USERS = "SELECT * FROM users ORDER BY id";
    private static final String SQL_SELECT_TEAMS = "SELECT * FROM teams ORDER BY id";
    private static final String SQL_SELECT_USER = "SELECT * FROM users WHERE login =?";
    private static final String SQL_SELECT_TEAM = "SELECT * FROM teams WHERE name =?";
    private static final String SQL_SELECT_USER_TEAMS = "SELECT ut.*, t.*  " +
            "FROM users_teams ut JOIN teams t ON ut.team_id=t.id WHERE user_id =? " +
            "";
    private static final String SQL_INSERT_USER = "INSERT INTO users VALUES (DEFAULT, ?)";
    private static final String SQL_INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";
    private static final String SQL_INSERT_TEAM_FOR_USER = "INSERT INTO users_teams VALUES (?, ?)";

    private static final String SQL_DELETE_USER = "DELETE FROM users WHERE login =?";
    private static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE name =?";
    private static final String SQL_UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    public static Connection getConnection() throws SQLException {
        Properties properties = new Properties();
        try (InputStream in = Files.newInputStream(Paths.get("app.properties"))) {
            properties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String urlConnect = properties.getProperty("connection.url");
        return DriverManager.getConnection(urlConnect);
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (Connection con = getConnection();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(SQL_SELECT_USERS)) {
            while (rs.next()) {
                users.add(extractUser(rs));
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        }
        return users;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection con = getConnection();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(SQL_SELECT_TEAMS)) {
            while (rs.next()) {
                teams.add(extractTeam(rs));
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        }
        return teams;
    }

    public User getUser(String login) throws DBException {
        User user = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            pstmt = con.prepareStatement(SQL_SELECT_USER);
            pstmt.setString(1, login);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                user = extractUser(rs);
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        } finally {
            close(con, pstmt, rs);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            pstmt = con.prepareStatement(SQL_SELECT_TEAM);
            pstmt.setString(1, name);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                team = extractTeam(rs);
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        } finally {
            close(con, pstmt, rs);
        }
        return team;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> userTeams = new ArrayList<>();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            pstmt = con.prepareStatement(SQL_SELECT_USER_TEAMS);
            pstmt.setInt(1, user.getId());
            rs = pstmt.executeQuery();
            while (rs.next()) {
                userTeams.add(extractTeam(rs));
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        } finally {
            close(con, pstmt, rs);
        }
        return userTeams;
    }

    public boolean insertUser(User user) throws DBException {
        boolean result = false;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            pstmt = con.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, user.getLogin());
            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    int userId = rs.getInt(1);
                    user.setId(userId);
                }
            }
            result = true;
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        } finally {
            close(con, pstmt, rs);
        }
        return result;
    }

    public boolean insertTeam(Team team) throws DBException {
        boolean result = false;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            pstmt = con.prepareStatement(SQL_INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, team.getName());
            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    int teamId = rs.getInt(1);
                    team.setId(teamId);
                }
            }
            result = true;
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        } finally {
            close(con, pstmt, rs);
        }
        return result;
    }

    public void setTeamsForUser(User user, Team... teams) throws DBException {
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = getConnection();
            con.setAutoCommit(false);
            pstmt = con.prepareStatement(SQL_INSERT_TEAM_FOR_USER);
            for (Team team : teams) {
                pstmt.setInt(1, user.getId());
                pstmt.setInt(2, team.getId());
                pstmt.executeUpdate();
            }
            con.commit();
        } catch (SQLException ex) {
            rollback(con);
            throw new DBException(ex.getMessage(), ex.getCause());
        } finally {
            close(con);
            close(pstmt);
        }
    }

    public void updateTeam(Team team) throws DBException {
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(SQL_UPDATE_TEAM)) {
            pstmt.setString(1, team.getName());
            pstmt.setInt(2, team.getId());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        }
    }

    public void deleteTeam(Team team) {
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(SQL_DELETE_TEAM)) {
            pstmt.setString(1, team.getName());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void deleteUsers(User... users) throws DBException {
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(SQL_DELETE_USER)) {
            for (User user : users) {
                pstmt.setString(1, user.getLogin());
                pstmt.executeUpdate();
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        }
    }

    private static User extractUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setLogin(rs.getString("login"));
        return user;
    }

    private static Team extractTeam(ResultSet rs) throws SQLException {
        Team team = new Team();
        team.setId(rs.getInt("id"));
        team.setName(rs.getString("name"));
        return team;
    }

    private static void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private static void close(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private static void close(Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private static void close(Connection con) {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    private static void close(Connection con, Statement stmt, ResultSet rs) {
        close(rs);
        close(stmt);
        close(con);
    }
}
